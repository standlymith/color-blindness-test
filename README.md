# Online Color Blindness Test

The original [Ishihara color blindness test](https://www.colorblindnesstest.org) was introduced in early last century and since then, it is by far the most well known color vision deficiency test all around the world  Dr. Shinobu Ishihara from Japan produced three different test sets which are widely used and which all based on the same pseudoisochromatic plates.

This test is actually designed to be used in a booklet and is usually executed by an eye doctor.But I have made an onlince version of the test, available right here on Colblindor.

The online test is based on the 38 plates edition and will give you a little feedback at the end of the test.

As this test is only made to check for red-green color blindness, any other form of CVD can not be detected. And at the end—if you like—you can even share your test result with your friends.This way they can see how you performed and try the test themselves if they like to.

**Test your color vision**

The test opposite, based on the Ishihara plates, is designed to give a quick assessment of color vision, and should not replace evaluation by a professional!

The original card version of Ishihara's color test was designed to be carried out in a room adequately lit by daylight. This electronic version may produce some discrepancies in the result as the images have been optimised for web-based delivery and with a 256 color display or greater. The results of this test are not to be considered a valid medical test for CVD and merely serve to illustrate the tests available.

Position yourself about 75cm from your monitor so that the color test image (a dotted disc) you are looking at is at eye level; then, select an image (Test A, B, C, D...) which contains a 1 or 2-digit number, or a shape, and see what you can see! Write your answer into the corresponding textbox. When you are finished, click on the submit button to see your score and compare your responses with the correct answers.

Color blindness is an inaccurate term to describe a lack of perceptual sensitivity to certain colors, a more precise term is Color Vision Deficiency (CVD). Color blindness is, however, the most commonly used term though it is misleading if taken literally, because colorblind people CAN see colors, but cannot make out the difference between some couples of complementary colors. Color vision deficiency is not related to visual acuity at all and is most commonly due to an inherited condition. Red/Green color vision deficiency is by far the most common form, about 99%, and causes problems in distinguishing reds and greens. Another color vision deficiency Blue/Yellow also exists, but is rare and there is no commonly available test for it.

- Color vision deficiency seems to occur in about 8% - 12% of males of European origin and about one-half of 1% of females. Total CVD (seeing in only shades of gray) is extremely rare.
- Another rare form of CVD called unilateral dichromacy affects people who have one normal eye and one colorblind eye.
- There is no treatment for color vision deficiency, nor is it usually the cause of any significant disability. Actually, most color deficient persons compensate well for their defect. At one time the U.S. Army found that colorblind persons can spot 'camouflage' colors where those with normal color vision are fooled by them.


